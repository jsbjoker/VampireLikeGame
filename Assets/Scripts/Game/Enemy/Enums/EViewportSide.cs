namespace Game.Enemy.Enums
{
    public enum EViewportSide
    {
        Up = 1,
        Right = 2,
        Down = -1,
        Left = -2
    }
}