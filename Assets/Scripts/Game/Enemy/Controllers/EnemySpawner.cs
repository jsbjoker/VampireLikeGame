using System;
using System.Linq;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Enemy.Interfaces;
using Game.World.Enums;
using Game.World.Interfaces;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Enemy.Controllers
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] private WorldState _worldState;
        [SerializeField] private CharacterFactory _characterFactory;
        [Tooltip("Must be positive and bigger then 0")]
        [SerializeField] private float _different;
        private float _spawnTimer;
        
        private IEnemyPositionGenerator _enemyPositionGenerator;

        private void Awake()
        {
            _enemyPositionGenerator = new EnemyViewPortGenerator(Camera.main, _different);
            _worldState.WorldStateChange += OnWorldStateChange;
        }

        private void OnWorldStateChange(EWorldState state)
        {
            if (state != EWorldState.Play) return;
            var enemies = ((ECharacterType[]) Enum.GetValues(typeof(ECharacterType))).ToList();
            enemies.Remove(ECharacterType.AdventureMan);
            for (var i = 0; i < 1; i++)
            {
                var enemy = enemies[Random.Range(0, enemies.Count)];
                _characterFactory.CreateCharacter(ETeam.Enemy, enemy,
                    _enemyPositionGenerator.GenerateEnemyPosition());
            }
        }

        private void Update()
        {
            // if (_worldState.CurrentState != EWorldState.Play) return;
            // _spawnTimer -= Time.deltaTime;
            // if (_spawnTimer > 0f) return;
            // _characterFactory.CreateCharacter(ETeam.Enemy, ECharacterType.MeleeEnemy,
            //     _enemyPositionGenerator.GenerateEnemyPosition());
            // _spawnTimer = Random.Range(_minSecondsSpawn, _maxSecondsSpawn);
        }
    }
}