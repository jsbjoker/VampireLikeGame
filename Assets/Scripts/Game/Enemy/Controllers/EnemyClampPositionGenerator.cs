using System;
using System.Collections.Generic;
using System.Linq;
using Game.Enemy.Enums;
using Game.Enemy.Interfaces;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = System.Random;

namespace Game.Enemy.Controllers
{
    public class EnemyClampPositionGenerator : IEnemyPositionGenerator
    {
        private readonly Random _random;
        private readonly Dictionary<EViewportSide, Func<Vector3>> _viewportsGenerators;
        private readonly List<EViewportSide> _viewportSides;
        private readonly Camera _mainCamera;
        private readonly int _tilemapOffset;
        private readonly BoundsInt _tilemapBounds;
        private readonly Vector2 _clampValues = new (0.2f, 0.8f);

        public EnemyClampPositionGenerator(Tilemap tilemap, int offset)
        {
            _tilemapBounds = tilemap.cellBounds;
            _tilemapOffset = offset;
            _random = new Random();
            _viewportsGenerators = new Dictionary<EViewportSide, Func<Vector3>>
            {
                [EViewportSide.Up] = GenerateUpViewport,
                [EViewportSide.Down] = GenerateDownViewport,
                [EViewportSide.Left] = GenerateLeftViewport,
                [EViewportSide.Right] = GenerateRightViewport,
            };
            _viewportSides = _viewportsGenerators.Keys.ToList();
            _mainCamera = Camera.main;
        }

        private Vector3 GenerateUpViewport() => new (RandomValue, 1.1f);
        private Vector3 GenerateDownViewport() => new (RandomValue, -0.1f);
        private Vector3 GenerateLeftViewport() => new (-0.1f, RandomValue);
        private Vector3 GenerateRightViewport() => new (1.1f, RandomValue);
        
        private float RandomValue => _clampValues.x + (float)_random.NextDouble() * (_clampValues.y - _clampValues.x);
        
        public Vector3 GenerateEnemyPosition()
        {
            var randomSide = _viewportSides[_random.Next(0, _viewportSides.Count)];
            var viewport = _viewportsGenerators[randomSide].Invoke();
            var position = ViewportToWorld(viewport);
            return IsValidPosition(position) ? position : ViewportToWorld(InvertViewport(viewport));
        }
        
        private bool IsValidPosition(Vector3 position)
        {
            return _tilemapBounds.xMax - _tilemapOffset > position.x &&
                   _tilemapBounds.xMin + _tilemapOffset < position.x &&
                   _tilemapBounds.yMax - _tilemapOffset > position.y &&
                   _tilemapBounds.yMin + _tilemapOffset < position.y;
        }
        
        private Vector3 InvertViewport(Vector3 viewport)
        {
            if (viewport.x is <= 0f or >= 1f)
                viewport.x = InvertValue(viewport.x);
            else
                viewport.y = InvertValue(viewport.y);
            return viewport;
        }

        private float InvertValue(float value) => value >= 1.1f ? -0.1f : 1.1f;

        private Vector3 ViewportToWorld(Vector3 viewport)
        {
            var worldPos = _mainCamera.ViewportToWorldPoint(viewport);
            worldPos.z = 0;
            return worldPos;
        }
    }
}