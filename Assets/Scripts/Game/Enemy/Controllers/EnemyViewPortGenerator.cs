using Game.Enemy.Interfaces;
using UnityEngine;
using Random = System.Random;

namespace Game.Enemy.Controllers
{
    public class EnemyViewPortGenerator : IEnemyPositionGenerator
    {
        private readonly Camera _camera;
        private readonly float _different;
        private readonly Random _random;

        public EnemyViewPortGenerator(Camera camera, float different)
        {
            _camera = camera;
            _different = different;
            _random = new Random();
        }

        private float GetViewPortPosition()
        {
            var sign = _random.Next(2);
            var randomValue = (float) _random.NextDouble();
            return sign > 0 ? randomValue * _different + 1.1f : randomValue * -_different - 0.1f;
        }


        public Vector3 GenerateEnemyPosition()
        {
            var xViewPortPosition = GetViewPortPosition();
            var yViewPortPosition = GetViewPortPosition();
            var position = _camera.ViewportToWorldPoint(new Vector3(xViewPortPosition, yViewPortPosition));
            position.z = 0;
            return position;
        }
    }
}