using UnityEngine;

namespace Game.Enemy.Interfaces
{
    public interface IEnemyPositionGenerator
    {
        Vector3 GenerateEnemyPosition();
    }
}