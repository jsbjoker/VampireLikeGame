using System;
using System.Collections.Generic;
using Game.Characters.Interfaces;
using Game.Inventory.Interfaces;

namespace Game.Inventory.Controllers
{
    public class InventoryStorageImp : InventoryStorage
    {
        private readonly Dictionary<ICharacter, IInventory> _inventories = new ();

        public override IInventory this[ICharacter character] => _inventories.TryGetValue(character, out var inventory)
            ? inventory
            : throw new NullReferenceException("Inventory not found " + character.CharacterType + character.CharacterID);

        public override void AddInventory(ICharacter character, IInventory inventory)
        {
            if (_inventories.ContainsKey(character)) return;
            _inventories.Add(character, inventory);
        }

        public override void RemoveInventory(ICharacter character)
        {
            if (!_inventories.ContainsKey(character)) return;
            _inventories.Remove(character);
        }
    }
}