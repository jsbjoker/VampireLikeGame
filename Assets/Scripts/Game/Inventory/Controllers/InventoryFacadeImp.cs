using Game.Inventory.Interfaces;
using UnityEngine;

namespace Game.Inventory.Controllers
{
    public class InventoryFacadeImp : InventoryFacade
    {
        [SerializeField] private InventoryStorage _inventoryStorage;
        [SerializeField] private InventoryFactory _inventoryFactory;

        public override InventoryStorage InventoryStorage => _inventoryStorage;
        public override InventoryFactory InventoryFactory => _inventoryFactory;
    }
}