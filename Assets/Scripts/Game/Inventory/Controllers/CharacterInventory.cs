using System;
using System.Collections.Generic;
using Game.Inventory.Enums;
using Game.Inventory.Interfaces;

namespace Game.Inventory.Controllers
{
    public class CharacterInventory : IInventory
    {
        private readonly Dictionary<EItem, IItem> _items;
        public IReadOnlyDictionary<EItem, IItem> Items => _items;

        public IItem this[EItem itemType] => _items.TryGetValue(itemType, out var item)
            ? item
            : throw new NullReferenceException("Item not found " + itemType);

        public CharacterInventory()
        {
            _items = new Dictionary<EItem, IItem>();
        }
        
        public void Add(IItem item)
        {
            if (_items.ContainsKey(item.ItemType)) return;
            _items[item.ItemType] = item;
        }

        public void Remove(IItem item)
        {
            if (!_items.ContainsKey(item.ItemType)) return;
            _items.Remove(item.ItemType);
        }
    }
}