using Game.Characters.Interfaces;
using Game.Inventory.Interfaces;
using UnityEngine;

namespace Game.Inventory.Controllers
{
    public class InventoryFactoryImp : InventoryFactory
    {
        [SerializeField] private InventoryStorage _inventoryStorage;
        
        public override IInventory CreateInventory(ICharacter character)
        {
            var inventory = new CharacterInventory();
            _inventoryStorage.AddInventory(character, inventory);
            return inventory;
        }
    }
}