using Game.Characters.Interfaces;
using UnityEngine;

namespace Game.Inventory.Interfaces
{
    public abstract class InventoryStorage : MonoBehaviour
    {
        public abstract IInventory this[ICharacter character] { get; }
        public abstract void AddInventory(ICharacter character, IInventory inventory);
        public abstract void RemoveInventory(ICharacter character);
    }
}