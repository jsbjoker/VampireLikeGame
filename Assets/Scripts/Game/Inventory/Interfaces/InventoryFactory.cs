using Game.Characters.Interfaces;
using UnityEngine;

namespace Game.Inventory.Interfaces
{
    public abstract class InventoryFactory : MonoBehaviour
    {
        public abstract IInventory CreateInventory(ICharacter character);
    }
}