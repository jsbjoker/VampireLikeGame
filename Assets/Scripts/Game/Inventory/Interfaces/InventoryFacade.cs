using UnityEngine;

namespace Game.Inventory.Interfaces
{
    public abstract class InventoryFacade : MonoBehaviour
    {
        public abstract InventoryStorage InventoryStorage { get; }
        public abstract InventoryFactory InventoryFactory { get; }
    }
}