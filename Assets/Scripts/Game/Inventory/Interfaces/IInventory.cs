using System.Collections.Generic;
using Game.Inventory.Enums;

namespace Game.Inventory.Interfaces
{
    public interface IInventory
    {
        IReadOnlyDictionary<EItem, IItem> Items { get; }
        IItem this[EItem itemType] { get; }
        void Add(IItem item);
        void Remove(IItem removeItem);

    }
}