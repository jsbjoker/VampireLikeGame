using Game.Animation.Interfaces;
using UnityEngine;

namespace Game.Animation.Controllers
{
    public class CharacterAnimatorFacadeImp : CharacterAnimatorFacade
    {
        [SerializeField] private CharacterAnimatorFactory _characterAnimatorFactory;
        [SerializeField] private CharacterAnimatorStorage _characterAnimatorStorage;

        public override CharacterAnimatorFactory CharacterAnimatorFactory => _characterAnimatorFactory;
        public override CharacterAnimatorStorage CharacterAnimatorStorage => _characterAnimatorStorage;
    }
}