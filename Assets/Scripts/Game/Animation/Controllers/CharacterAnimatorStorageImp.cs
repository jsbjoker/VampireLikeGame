using System;
using System.Collections.Generic;
using Game.Animation.Interfaces;
using Game.Characters.Interfaces;

namespace Game.Animation.Controllers
{
    public class CharacterAnimatorStorageImp : CharacterAnimatorStorage
    {
        private readonly Dictionary<ICharacter, ICharacterAnimator> _characterAnimators = new ();

        public override ICharacterAnimator this[ICharacter character] => _characterAnimators.ContainsKey(character)
            ? _characterAnimators[character]
            : throw new NullReferenceException("Character animator not found " + character.CharacterID);

        public override void AddCharacterAnimator(ICharacter character, ICharacterAnimator characterAnimator)
        {
            if (_characterAnimators.ContainsKey(character)) return;
            _characterAnimators.Add(character, characterAnimator);
        }

        public override void RemoveCharacterAnimator(ICharacter character)
        {
            if (!_characterAnimators.ContainsKey(character)) return;
            _characterAnimators[character].Dispose();
            _characterAnimators.Remove(character);
        }
    }
}