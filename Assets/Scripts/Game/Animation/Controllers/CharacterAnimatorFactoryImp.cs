using System;
using System.Collections.Generic;
using System.Linq;
using Game.Animation.Enums;
using Game.Animation.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Characters.Views;
using UnityEngine;

namespace Game.Animation.Controllers
{
    public class CharacterAnimatorFactoryImp : CharacterAnimatorFactory
    {
        [SerializeField] private CharacterAnimatorStorage _characterAnimatorStorage;
        
        public override ICharacterAnimator CreateCharacterAnimator(ICharacter character, CharacterView characterView)
        {
            var animator = characterView.GetComponent<Animator>();
            var characterAnimator = new CharacterAnimator(animator, characterView,
                character.Team == ETeam.Player ? GetAnimationsLenght(animator) : new Dictionary<EAnimationType, float>());
            _characterAnimatorStorage.AddCharacterAnimator(character, characterAnimator);
            return characterAnimator;
        }

        private Dictionary<EAnimationType, float> GetAnimationsLenght(Animator animator)
        {
            var animationLength = new Dictionary<EAnimationType, float>();
            foreach (EAnimationType animType in Enum.GetValues(typeof(EAnimationType)))
            {
                var clip = animator.runtimeAnimatorController.animationClips.FirstOrDefault(x =>
                    x.name.Contains(animType.ToString()));
                if (clip == null) continue;
                animationLength.Add(animType, clip.length);
            }
            return animationLength;
        }
    }
}