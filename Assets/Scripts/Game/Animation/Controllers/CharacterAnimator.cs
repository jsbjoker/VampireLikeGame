using System;
using System.Collections.Generic;
using Game.Animation.Enums;
using Game.Animation.Interfaces;
using UnityEngine;

namespace Game.Animation.Controllers
{
    public class CharacterAnimator : ICharacterAnimator
    {
        private readonly Animator _animator;
        private readonly IAnimationEventHandler _animationEventHandler;
        private readonly Dictionary<EAnimationType, Action> _animationCallbacks;
        private readonly Dictionary<EAnimationType, int> _animationHashes;
        private readonly Dictionary<EAnimationType, float> _animationLength;

        public CharacterAnimator(Animator animator, IAnimationEventHandler animationEventHandler,
            Dictionary<EAnimationType, float> animationLength)
        {
            _animator = animator;
            _animationEventHandler = animationEventHandler;
            _animationHashes = new Dictionary<EAnimationType, int>
            {
                [EAnimationType.Run] = Animator.StringToHash("Run"),
                [EAnimationType.Die] = Animator.StringToHash("Die")
            };
            _animationCallbacks = new Dictionary<EAnimationType, Action>();
            _animationLength = animationLength;
            _animationEventHandler.AnimationEvent += OnAnimationEvent;
        }

        public void SetBool(EAnimationType animType, bool value)
        {
            if (!_animationHashes.ContainsKey(animType)) return;
            _animator.SetBool(_animationHashes[animType], value);
        }

        public void SetTrigger(EAnimationType animType)
        {
            SetTrigger(animType, null);
        }

        public void SetTrigger(EAnimationType animType, Action animationCallback)
        {
            if (!_animationHashes.ContainsKey(animType)) return;
            _animationCallbacks[animType] = animationCallback;
            _animator.SetTrigger(_animationHashes[animType]);
        }

        public void SetSpeed(float speed) => _animator.speed = speed;

        public float GetAnimationDuration(EAnimationType animation) =>
            _animationLength.ContainsKey(animation) ? _animationLength[animation] : 0f;

        private void OnAnimationEvent(EAnimationType animType)
        {
            if (!_animationCallbacks.ContainsKey(animType)) return;
            _animationCallbacks[animType]?.Invoke();
            _animationCallbacks.Remove(animType);
        }
        
        public void Dispose()
        {
            _animationEventHandler.AnimationEvent -= OnAnimationEvent;
        }
    }
}