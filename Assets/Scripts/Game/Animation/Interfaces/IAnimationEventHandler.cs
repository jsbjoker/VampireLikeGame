using System;
using Game.Animation.Enums;

namespace Game.Animation.Interfaces
{
    public interface IAnimationEventHandler
    {
        event Action<EAnimationType> AnimationEvent;
    }
}