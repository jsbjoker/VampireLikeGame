using UnityEngine;

namespace Game.Animation.Interfaces
{
    public abstract class CharacterAnimatorFacade : MonoBehaviour
    {
        public abstract CharacterAnimatorFactory CharacterAnimatorFactory { get; }
        public abstract CharacterAnimatorStorage CharacterAnimatorStorage { get; }
    }
}