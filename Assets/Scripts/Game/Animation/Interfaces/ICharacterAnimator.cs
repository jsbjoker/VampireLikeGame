using System;
using Game.Animation.Enums;

namespace Game.Animation.Interfaces
{
    public interface ICharacterAnimator : IDisposable
    {
        void SetBool(EAnimationType animType, bool value);
        void SetTrigger(EAnimationType animType);
        void SetTrigger(EAnimationType animType, Action callbackAction);
        void SetSpeed(float speed);
        float GetAnimationDuration(EAnimationType animation);
    }
}