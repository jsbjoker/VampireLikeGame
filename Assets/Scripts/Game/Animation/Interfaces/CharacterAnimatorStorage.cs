using Game.Characters.Interfaces;
using UnityEngine;

namespace Game.Animation.Interfaces
{
    public abstract class CharacterAnimatorStorage : MonoBehaviour
    {
        public abstract ICharacterAnimator this[ICharacter character] { get; }
        public abstract void AddCharacterAnimator(ICharacter character, ICharacterAnimator characterAnimator);
        public abstract void RemoveCharacterAnimator(ICharacter character);
    }
}