using Game.Characters.Interfaces;
using Game.Characters.Views;
using UnityEngine;

namespace Game.Animation.Interfaces
{
    public abstract class CharacterAnimatorFactory : MonoBehaviour
    {
        public abstract ICharacterAnimator CreateCharacterAnimator(ICharacter character, CharacterView characterView);
    }
}