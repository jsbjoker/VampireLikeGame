namespace Game.Player.Interfaces
{
    public interface IPlayerHealthView
    {
        void SetPlayerHealth(float currentHealth, float maxHealth);
    }
}