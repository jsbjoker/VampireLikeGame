using Game.Player.Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Player.Views
{
    public class PlayerHealthView : MonoBehaviour, IPlayerHealthView
    {
        [SerializeField] private TextMeshProUGUI _healthText;
        [SerializeField] private Image _healthBar;
        
        
        public void SetPlayerHealth(float currentHealth, float maxHealth)
        {
            _healthText.text = $"{Mathf.Round(currentHealth)}/{Mathf.Round(maxHealth)}";
            _healthBar.fillAmount = currentHealth / maxHealth;
        }
    }
}