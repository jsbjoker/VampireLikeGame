using Cinemachine;
using Common.InputHandlers.Interfaces;
using Common.Mediators.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Player.Interfaces;
using Game.Player.Views;
using Game.World.Enums;
using Game.World.Interfaces;
using UnityEngine;

namespace Game.Player.Controllers
{
    public class PlayerInitializerImp : PlayerInitializer
    {
        [SerializeField] private CharacterFacade _characterFacade;
        [SerializeField] private CinemachineVirtualCamera _cinemachineVirtualCamera;
        [SerializeField] private InputHandler _inputHandler;
        [SerializeField] private MediatorFacade _mediatorFacade;
        [SerializeField] private WorldState _worldState;
        [SerializeField] private PlayerHealthView _playerHealthView;
        private IPlayerHealthBarPresenter _healthBarPresenter;
        

        private void Awake()
        {
            _worldState.WorldStateChange += InitializePlayer;
        }

        private void InitializePlayer(EWorldState worldState)
        {
            if (worldState != EWorldState.Initialize) return;
            _worldState.WorldStateChange -= InitializePlayer;
            var player = _characterFacade.CharacterFactory.CreateCharacter(ETeam.Player, ECharacterType.AdventureMan);
            CreatePlayerHealthBarPresenter(player);
            _cinemachineVirtualCamera.Follow =
                _characterFacade.CharacterTransformStorage.GetTransformByCharacter(player);
        }

        private void CreatePlayerHealthBarPresenter(ICharacter player)
        {
            _healthBarPresenter = new PlayerHealthBarPresenter(player, _playerHealthView);
        }

        private void OnDestroy()
        {
            _healthBarPresenter?.Dispose();
            _worldState.WorldStateChange -= InitializePlayer;
        }
    }
}