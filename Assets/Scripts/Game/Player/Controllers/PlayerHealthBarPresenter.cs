using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Player.Interfaces;

namespace Game.Player.Controllers
{
    public class PlayerHealthBarPresenter : IPlayerHealthBarPresenter
    {
        private readonly ICharacter _player;
        private readonly IPlayerHealthView _playerHealthView;

        public PlayerHealthBarPresenter(ICharacter player, IPlayerHealthView playerHealthView)
        {
            _player = player;
            _playerHealthView = playerHealthView;
            _player.Model.CharacterParamChange += TrySetupHealthBar;
            TrySetupHealthBar(ECharacterParam.Health, 0f);
        }

        private void TrySetupHealthBar(ECharacterParam param, float value)
        {
            if (!IsValidParam(param)) return;
            _playerHealthView.SetPlayerHealth(_player.Model[ECharacterParam.Health], _player.Model[ECharacterParam.MaxHealth]);
        }

        private bool IsValidParam(ECharacterParam param) =>
            param is ECharacterParam.Health or ECharacterParam.MaxHealth;

        public void Dispose()
        {
            _player.Model.CharacterParamChange -= TrySetupHealthBar;
        }
    }
}