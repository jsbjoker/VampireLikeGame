using System;
using Game.World.Enums;
using UnityEngine;

namespace Game.World.Interfaces
{
    public abstract class WorldState : MonoBehaviour
    {
        public abstract event Action<EWorldState> WorldStateChange; 
        public abstract EWorldState CurrentState { get; }
        public abstract void ChangeWorldState(EWorldState worldState);
    }
}