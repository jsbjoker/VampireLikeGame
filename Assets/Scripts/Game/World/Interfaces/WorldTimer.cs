using System;
using UnityEngine;

namespace Game.World.Interfaces
{
    public abstract class WorldTimer : MonoBehaviour
    {
        public abstract event Action<TimeSpan> WorldTimeChange;
    }
}