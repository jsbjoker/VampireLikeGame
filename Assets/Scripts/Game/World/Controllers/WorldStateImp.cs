using System;
using Game.World.Enums;
using Game.World.Interfaces;

namespace Game.World.Controllers
{
    public class WorldStateImp : WorldState
    {
        private EWorldState _currentState = EWorldState.None;
        public override EWorldState CurrentState => _currentState;
        
        public override event Action<EWorldState> WorldStateChange;

        private void Start()
        {
            ChangeWorldState(EWorldState.Initialize);
            ChangeWorldState(EWorldState.Play);
        }

        public override void ChangeWorldState(EWorldState worldState)
        {
            if (CurrentState == worldState) return;
            _currentState = worldState;
            WorldStateChange?.Invoke(worldState);
        }

        private void OnDestroy()
        {
            WorldStateChange = null;
        }
    }
}