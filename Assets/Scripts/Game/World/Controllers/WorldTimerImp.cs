using System;
using Game.World.Enums;
using Game.World.Interfaces;
using UnityEngine;

namespace Game.World.Controllers
{
    public class WorldTimerImp : WorldTimer
    {
        [SerializeField] private WorldState _worldState;
        private readonly TimeSpan _refreshTime = new TimeSpan(0, 0, 1);
        private TimeSpan _worldTime;
        private float _timer;
        
        public override event Action<TimeSpan> WorldTimeChange;

        private void Awake()
        {
            _worldTime = TimeSpan.Zero;
            _timer = _refreshTime.Seconds;
        }

        private void Start()
        {
            WorldTimeChange?.Invoke(_worldTime);
        }

        private void Update()
        {
            if (_worldState.CurrentState != EWorldState.Play) return;
            _timer -= Time.deltaTime;
            if (_timer > 0f) return;
            _worldTime += _refreshTime;
            WorldTimeChange?.Invoke(_worldTime);
            _timer = _refreshTime.Seconds;
        }

        private void OnDestroy()
        {
            WorldTimeChange = null;
        }
    }
}