using System;
using Game.World.Interfaces;
using TMPro;
using UnityEngine;

namespace Game.World.Controllers
{
    public class WorldTimerPresenterImp : WorldTimerPresenter
    {
        [SerializeField] private TextMeshProUGUI _timerText;
        [SerializeField] private WorldTimer _worldTimer;

        private void Awake()
        {
            _worldTimer.WorldTimeChange += OnWorldTimerChange;
        }

        private void OnWorldTimerChange(TimeSpan timer)
        {
            _timerText.text = $"{timer.Minutes:00} : {timer.Seconds:00}";
        }

        private void OnDestroy()
        {
            _worldTimer.WorldTimeChange -= OnWorldTimerChange;
        }
    }
}