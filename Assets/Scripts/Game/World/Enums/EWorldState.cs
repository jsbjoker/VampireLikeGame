namespace Game.World.Enums
{
    public enum EWorldState
    {
        None,
        Initialize,
        Play,
        Pause,
        EndGame
    }
}