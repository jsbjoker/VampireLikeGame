using System;
using System.Collections.Generic;
using System.Linq;
using Common.Config.Enums;
using Common.Config.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using UnityEngine;

namespace Game.Characters.Controllers
{
    public class CharacterDataProviderImp : CharacterDataProvider
    {
        [SerializeField] private ConfigProvider _configProvider;
        private Dictionary<ECharacterType, Dictionary<ECharacterParam, float>> _characterParams;

        private void Awake()
        {
            _characterParams =
                _configProvider.GetConfig<Dictionary<ECharacterType, Dictionary<ECharacterParam, float>>>(
                    EConfig.CharacterParamsConfig);
        }

        public override Dictionary<ECharacterParam, float> GetCharacterParams(ECharacterType character)
        {
            return _characterParams.TryGetValue(character, out var param)
                ? param.ToDictionary(k => k.Key, v => v.Value)
                : throw new NullReferenceException("Character params not found " + character);
        }
    }
}