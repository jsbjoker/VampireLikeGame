using Game.Animation.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Movement.Interfaces;
using Game.StateMachine.Interfaces;
using UnityEngine;

namespace Game.Characters.Controllers
{
    public class CharacterDisposeImp : CharacterDispose
    {
        [SerializeField] private CharacterFacade _characterFacade;
        [SerializeField] private CharacterAnimatorFacade _characterAnimatorFacade;
        [SerializeField] private CharacterMovementFacade _characterMovementFacade;
        [SerializeField] private CharacterStateMachineFacade _characterStateMachineFacade;
        
        
        public override void DisposeCharacter(ICharacter character)
        {
            if (character.Team == ETeam.Player)
                DisposePlayer(character);
            else
                DisposeEnemy(character);
            character.CharacterDie -= DisposePlayer;
        }

        private void DisposePlayer(ICharacter character)
        {
            BaseDispose(character);
        }
        
        private void DisposeEnemy(ICharacter character)
        {
            BaseDispose(character);
        }
        
        private void BaseDispose(ICharacter character)
        {
            _characterFacade.CharacterTransformStorage.RemoveCharacterTransform(character);
            _characterAnimatorFacade.CharacterAnimatorStorage.RemoveCharacterAnimator(character);
            _characterMovementFacade.CharacterMovementStorage.RemoveCharacterMovements(character);
            _characterStateMachineFacade.CharacterStateMachineStorage.RemoveCharacterStateMachine(character);
            _characterFacade.CharacterStorage.RemoveCharacter(character);
        }
    }
}