using System;
using System.Collections.Generic;
using Game.Characters.Enums;
using Game.Characters.Interfaces;

namespace Game.Characters.Controllers
{
    public class CharacterModel : ICharacterModel
    {
        private readonly Dictionary<ECharacterParam, float> _characterParams;

        public CharacterModel(Dictionary<ECharacterParam, float> characterParams)
        {
            _characterParams = characterParams;
        }

        public event Action<ECharacterParam, float> CharacterParamChange;

        public float this[ECharacterParam param] => _characterParams.ContainsKey(param) ? _characterParams[param] : 0f;

        public bool IsAlive => _characterParams.ContainsKey(ECharacterParam.Health) &&
                               _characterParams[ECharacterParam.Health] > 0f;

        public IReadOnlyDictionary<ECharacterParam, float> CharacterParams => _characterParams;

        public void SetCharacterParam(ECharacterParam param, float value)
        {
            _characterParams[param] = value;
            CharacterParamChange?.Invoke(param, value);
        }

        public void Dispose()
        {
            CharacterParamChange = null;
        }
    }
}