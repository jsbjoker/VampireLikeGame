using System;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using UnityEngine;

namespace Game.Characters.Controllers
{
    public class Character : IDeadly
    {
        private readonly Collider2D _collider;
        private readonly ICollisionHandler _collisionHandler;
        public ETeam Team { get; }
        public ECharacterType CharacterType { get; }
        public int CharacterID { get; }
        public ICharacterModel Model { get; }
        
        public event Action<Transform> CollisionEnter;
        public event Action<Transform> CollisionStay;
        public event Action<Transform> CollisionExit;
        
        public event Action<ICharacter> CharacterDie;
        public Character(ETeam team, ECharacterType characterType, int characterID, ICharacterModel model,
            ICollisionHandler collisionHandler, Collider2D collider)
        {
            _collisionHandler = collisionHandler;
            _collider = collider;
            Team = team;
            CharacterType = characterType;
            CharacterID = characterID;
            Model = model;
            _collisionHandler.CollisionEnter += InvokeCollisionEnter;
            _collisionHandler.CollisionStay += InvokeCollisionStay;
            _collisionHandler.CollisionExit += InvokeCollisionExit;
        }

        private void InvokeCollisionEnter(Transform transform)
        {
            CollisionEnter?.Invoke(transform);
        }
        
        private void InvokeCollisionStay(Transform transform)
        {
            CollisionStay?.Invoke(transform);
        }
        
        private void InvokeCollisionExit(Transform transform)
        {
            CollisionExit?.Invoke(transform);
        }

        public void SetActiveCollider(bool value)
        {
            _collider.enabled = value;
        }

        public void Die()
        {
            CharacterDie?.Invoke(this);
            _collisionHandler.CollisionEnter -= InvokeCollisionEnter;
            _collisionHandler.CollisionStay -= InvokeCollisionStay;
            _collisionHandler.CollisionExit -= InvokeCollisionExit;
            CharacterDie = null;
        }
    }
}