using System;
using System.Collections.Generic;
using Game.Characters.Enums;
using Game.Characters.Interfaces;

namespace Game.Characters.Controllers
{
    public class CharacterStorageImp : CharacterStorage
    {
        private readonly Dictionary<int, ICharacter> _characters = new();
        private ICharacter _player;
        public override ICharacter Player => _player;
        
        public override event Action<ICharacter> CharacterCreate;

        public override ICharacter GetCharacterByID(int characterID)
        {
            return _characters.ContainsKey(characterID)
                ? _characters[characterID]
                : throw new NullReferenceException("Character with ID " + characterID + " not found");
        }

        public override void AddCharacter(ICharacter character)
        {
            if (character.Team == ETeam.Player)
                _player = character;
            else
                _characters.Add(character.CharacterID, character);
            CharacterCreate?.Invoke(character);
        }

        public override void RemoveCharacter(ICharacter character)
        {
            if (!_characters.ContainsKey(character.CharacterID)) return;
            _characters.Remove(character.CharacterID);
        }

        private void OnDestroy()
        {
            CharacterCreate = null;
        }
    }
}