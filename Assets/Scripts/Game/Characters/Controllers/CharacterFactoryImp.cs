using System;
using Game.Animation.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Characters.Settings;
using Game.Inventory.Interfaces;
using Game.Movement.Interfaces;
using Game.StateMachine.Interfaces;
using UnityEngine;
using Random = System.Random;

namespace Game.Characters.Controllers
{
    public class CharacterFactoryImp : CharacterFactory
    {
        [SerializeField] private CharacterSettings _characterSettings;
        [SerializeField] private CharacterDataProvider _characterDataProvider;
        [SerializeField] private CharacterFacade _characterFacade;
        [SerializeField] private CharacterDispose _characterDispose;
        [SerializeField] private CharacterAnimatorFactory _characterAnimatorFactory;
        [SerializeField] private CharacterStateMachineFactory _characterStateMachineFactory;
        [SerializeField] private CharacterMovementFactory _characterMovementFactory;
        [SerializeField] private InventoryFactory _inventoryFactory;
        private readonly Random _random = new();

        public override ICharacter CreateCharacter(ETeam team, ECharacterType characterType)
        {
            return CreateCharacter(team, characterType, Vector3.zero);
        }

        public override ICharacter CreateCharacter(ETeam team, ECharacterType characterType, Vector3 position)
        {
            if (!_characterSettings.ContainsCharacter(characterType)) 
                throw new NullReferenceException("Error with create character: Character " + characterType + " not found");
            
            var characterView = Instantiate(_characterSettings.GetCharacterByType(characterType), position, Quaternion.identity);
            var characterTransform = characterView.transform;
            var characterId = _random.Next(0, int.MaxValue);
            var characterModel = new CharacterModel(_characterDataProvider.GetCharacterParams(characterType));
            var character = new Character(team, characterType, characterId,
                characterModel, characterView, characterView.GetComponent<Collider2D>());
            
            _characterFacade.CharacterTransformStorage.AddCharacterTransform(character, characterTransform);
            _characterAnimatorFactory.CreateCharacterAnimator(character, characterView);
            _characterMovementFactory.CreateCharacterMovement(character, characterView);
            _inventoryFactory.CreateInventory(character);
            _characterStateMachineFactory.CreateCharacterStateMachine(character);
            _characterFacade.CharacterStorage.AddCharacter(character);
            character.CharacterDie += _characterDispose.DisposeCharacter;
            return character;
        }
    }
}