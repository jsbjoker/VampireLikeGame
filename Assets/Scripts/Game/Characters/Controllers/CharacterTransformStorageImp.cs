using System;
using System.Collections.Generic;
using Game.Characters.Interfaces;
using UnityEngine;

namespace Game.Characters.Controllers
{
    public class CharacterTransformStorageImp : CharacterTransformStorage
    {
        private readonly Dictionary<ICharacter, Transform> _character_transforms = new ();
        private readonly Dictionary<Transform, ICharacter> _transfrom_character = new ();


        public override ICharacter GetCharacterByTransform(Transform characterTransform) =>
            _transfrom_character.ContainsKey(characterTransform)
                ? _transfrom_character[characterTransform]
                : throw new NullReferenceException("Character by transform not found " + characterTransform.name);

        public override Transform GetTransformByCharacter(ICharacter character) =>
            _character_transforms.ContainsKey(character)
                ? _character_transforms[character]
                : throw new NullReferenceException("Transform by character not found " + character.CharacterID);

        public override void AddCharacterTransform(ICharacter character, Transform characterTransform)
        {
            if (_character_transforms.ContainsKey(character)) return;
            _character_transforms.Add(character, characterTransform);
            _transfrom_character.Add(characterTransform, character);
        }

        public override void RemoveCharacterTransform(ICharacter character)
        {
            if (!_character_transforms.ContainsKey(character)) return;
            var characterTransform = _character_transforms[character];
            _character_transforms.Remove(character);
            _transfrom_character.Remove(characterTransform);
        }
    }
}