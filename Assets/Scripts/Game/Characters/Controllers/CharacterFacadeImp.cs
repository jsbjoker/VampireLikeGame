using Game.Animation.Interfaces;
using Game.Characters.Interfaces;
using Game.Movement.Interfaces;
using Game.StateMachine.Interfaces;
using UnityEngine;

namespace Game.Characters.Controllers
{
    public class CharacterFacadeImp : CharacterFacade
    {
        [SerializeField] private CharacterStorage _characterStorage;
        [SerializeField] private CharacterFactory _characterFactory;
        [SerializeField] private CharacterTransformStorage _characterTransformStorage;
        public override CharacterFactory CharacterFactory => _characterFactory;
        public override CharacterStorage CharacterStorage => _characterStorage;
        public override CharacterTransformStorage CharacterTransformStorage => _characterTransformStorage;
    }
}