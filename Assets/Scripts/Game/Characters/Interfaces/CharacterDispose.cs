using UnityEngine;

namespace Game.Characters.Interfaces
{
    public abstract class CharacterDispose : MonoBehaviour
    {
        public abstract void DisposeCharacter(ICharacter character);
    }
}