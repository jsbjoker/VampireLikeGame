using UnityEngine;

namespace Game.Characters.Interfaces
{
    public abstract class CharacterTransformStorage : MonoBehaviour
    {
        public abstract ICharacter GetCharacterByTransform(Transform characterTransform);
        public abstract Transform GetTransformByCharacter(ICharacter character);
        public abstract void AddCharacterTransform(ICharacter character, Transform characterTransform);
        public abstract void RemoveCharacterTransform(ICharacter character);
    }
}