using UnityEngine;

namespace Game.Characters.Interfaces
{
    public abstract class CharacterFacade : MonoBehaviour
    {
        public abstract CharacterFactory CharacterFactory { get; }
        public abstract CharacterStorage CharacterStorage { get; }
        public abstract CharacterTransformStorage CharacterTransformStorage { get; }
    }
}