using System;
using Game.Characters.Enums;

namespace Game.Characters.Interfaces
{
    public interface ICharacter : ICollisionHandler
    {
        public ETeam Team { get; }
        public ECharacterType CharacterType { get; }
        public int CharacterID { get; }
        public ICharacterModel Model { get; }
        event Action<ICharacter> CharacterDie;
    }
}