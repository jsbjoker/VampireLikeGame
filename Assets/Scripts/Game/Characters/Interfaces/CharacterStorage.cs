using System;
using UnityEngine;

namespace Game.Characters.Interfaces
{
    public abstract class CharacterStorage : MonoBehaviour
    {
        public abstract event Action<ICharacter> CharacterCreate;
        public abstract ICharacter Player { get; }
        public abstract ICharacter GetCharacterByID(int characterID);
        public abstract void AddCharacter(ICharacter character);
        public abstract void RemoveCharacter(ICharacter character);
    }
}