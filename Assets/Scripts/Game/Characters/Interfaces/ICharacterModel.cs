using System;
using System.Collections.Generic;
using Game.Characters.Enums;

namespace Game.Characters.Interfaces
{
    public interface ICharacterModel : IDisposable
    {
        float this[ECharacterParam param] { get; }
        bool IsAlive { get; }
        IReadOnlyDictionary<ECharacterParam, float> CharacterParams { get; }
        event Action<ECharacterParam, float> CharacterParamChange;
        void SetCharacterParam(ECharacterParam param, float value);
    }
}