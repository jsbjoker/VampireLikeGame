using Game.Characters.Enums;
using UnityEngine;

namespace Game.Characters.Interfaces
{
    public abstract class CharacterFactory : MonoBehaviour
    {
        public abstract ICharacter CreateCharacter(ETeam team, ECharacterType characterType);
        public abstract ICharacter CreateCharacter(ETeam team, ECharacterType characterType, Vector3 position);
    }
}