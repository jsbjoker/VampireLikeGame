namespace Game.Characters.Interfaces
{
    public interface IDeadly : ICharacter
    {
        void SetActiveCollider(bool value);
        void Die();
    }
}