using System.Collections.Generic;
using Game.Characters.Enums;
using UnityEngine;

namespace Game.Characters.Interfaces
{
    public abstract class CharacterDataProvider : MonoBehaviour
    {
        public abstract Dictionary<ECharacterParam, float> GetCharacterParams(ECharacterType character);
    }
}