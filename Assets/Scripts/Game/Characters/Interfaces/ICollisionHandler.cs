using System;
using UnityEngine;

namespace Game.Characters.Interfaces
{
    public interface ICollisionHandler
    {
        event Action<Transform> CollisionEnter;
        event Action<Transform> CollisionStay;
        event Action<Transform> CollisionExit;
    }
}