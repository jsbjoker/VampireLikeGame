using System;
using Game.Animation.Enums;
using Game.Animation.Interfaces;
using Game.Characters.Interfaces;
using UnityEngine;

namespace Game.Characters.Views
{
    [RequireComponent(typeof(Animator), typeof(Collider2D))]
    public class CharacterView : MonoBehaviour, IAnimationEventHandler, ICollisionHandler
    {
        public event Action<Transform> CollisionEnter;
        public event Action<Transform> CollisionStay;
        public event Action<Transform> CollisionExit;
        public event Action<EAnimationType> AnimationEvent;

        private void OnCollisionEnter2D(Collision2D other)
        {
            CollisionEnter?.Invoke(other.transform);
        }

        private void OnCollisionStay2D(Collision2D other)
        {
            CollisionStay?.Invoke(other.transform);
        }

        private void OnCollisionExit2D(Collision2D other)
        {
            CollisionExit?.Invoke(other.transform);
        }

        /// <summary>
        /// Called by animation event
        /// </summary>
        /// <param name="animType"></param>
        public void AnimationCallback(EAnimationType animType)
        {
            AnimationEvent?.Invoke(animType);
        }

        private void OnDestroy()
        {
            CollisionEnter = null;
            CollisionExit = null;
            CollisionStay = null;
            AnimationEvent = null;
        }
    }
}