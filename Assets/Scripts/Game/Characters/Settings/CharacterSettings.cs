using System;
using Game.Characters.Enums;
using Game.Characters.Views;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Game.Characters.Settings
{
    [CreateAssetMenu(fileName = "CharacterSettings", menuName = "Settings/Game/CharacterSettings")]
    public class CharacterSettings : ScriptableObject
    {
        [SerializeField] private CharactersDictionary _charactersList;

        public CharacterView GetCharacterByType(ECharacterType characterType)
        {
            return _charactersList.ContainsKey(characterType)
                ? _charactersList[characterType]
                : throw new NullReferenceException("Character not found " + characterType);
        }

        public bool ContainsCharacter(ECharacterType characterType)
        {
            return _charactersList.ContainsKey(characterType);
        }
    }

    [Serializable]
    public class CharactersDictionary : SerializableDictionaryBase<ECharacterType, CharacterView>
    {
    }
}