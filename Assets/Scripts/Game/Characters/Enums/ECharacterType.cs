namespace Game.Characters.Enums
{
    public enum ECharacterType
    {
        AdventureMan,
        Skeleton = 5,
        FlyingEye = 10,
        Mushroom = 15,
        Goblin = 20
    }
}