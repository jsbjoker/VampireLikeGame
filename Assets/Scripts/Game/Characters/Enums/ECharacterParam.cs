namespace Game.Characters.Enums
{
    public enum ECharacterParam
    {
        Health,
        MaxHealth,
        MoveSpeed,
        AttackRange,
        Damage
    }
}