using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.EndGame.Interfaces;
using Game.World.Enums;
using Game.World.Interfaces;
using UnityEngine;

namespace Game.EndGame.Controllers
{
    public class EndGameHandlerImp : EndGameHandler
    {
        [SerializeField] private CharacterStorage _characterStorage;
        [SerializeField] private WorldState _worldState;
        private ICharacter _player;
        
        private void Awake()
        {
            _characterStorage.CharacterCreate += OnCharacterCreate;
        }

        private void OnCharacterCreate(ICharacter character)
        {
            if (character.Team == ETeam.Enemy) return;
            _player = character;
            _characterStorage.CharacterCreate -= OnCharacterCreate;
            _player.CharacterDie += OnCharacterDie;
        }

        private void OnCharacterDie(ICharacter character)
        {
            _worldState.ChangeWorldState(EWorldState.EndGame);
            _player.CharacterDie -= OnCharacterDie;
            _player = null;
        }

        private void OnDestroy()
        {
            _characterStorage.CharacterCreate -= OnCharacterCreate;
        }
    }
}