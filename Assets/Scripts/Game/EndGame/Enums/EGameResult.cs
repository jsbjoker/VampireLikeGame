namespace Game.EndGame.Enums
{
    public enum EGameResult
    {
        Win,
        Lose
    }
}