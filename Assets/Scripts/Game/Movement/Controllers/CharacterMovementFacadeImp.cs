using Game.Movement.Interfaces;
using UnityEngine;

namespace Game.Movement.Controllers
{
    public class CharacterMovementFacadeImp : CharacterMovementFacade
    {
        [SerializeField] private CharacterMovementFactory _characterMovementFactory;
        [SerializeField] private CharacterMovementStorage _characterMovementStorage;

        public override CharacterMovementFactory CharacterMovementFactory => _characterMovementFactory;
        public override CharacterMovementStorage CharacterMovementStorage => _characterMovementStorage;
    }
}