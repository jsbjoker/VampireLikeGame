using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Movement.Interfaces;
using UnityEngine;

namespace Game.Movement.Controllers
{
    public class CharacterPhysicMovement : ICharacterMovement
    {
        private readonly Transform _characterTransform;
        private readonly Rigidbody2D _rigidbody;
        private readonly ICharacterModel _model;

        public bool RightFacing { get; private set; }
        

        public bool IsMoving => _rigidbody.velocity != Vector2.zero;

        public CharacterPhysicMovement(Transform characterTransform, Rigidbody2D rigidbody, ICharacterModel model)
        {
            _characterTransform = characterTransform;
            _rigidbody = rigidbody;
            _model = model;
            RightFacing = true;
        }

        public void MoveTo(Vector2 direction)
        {
            _rigidbody.velocity = direction * _model[ECharacterParam.MoveSpeed];
            
            if (direction.x < 0f && RightFacing)
                FlipCharacter();

            if (direction.x > 0f && !RightFacing)
                FlipCharacter();
        }
        
        public void Stop()
        {
            _rigidbody.velocity = Vector2.zero;
        }

        private void FlipCharacter()
        {
            RightFacing = !RightFacing;
            var scale = _characterTransform.localScale;
            scale.x *= -1;
            _characterTransform.localScale = scale;
        }


        


    }
}