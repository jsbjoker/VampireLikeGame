using System;
using System.Collections.Generic;
using Common.Mediators.Interfaces;
using Game.Characters.Interfaces;
using Game.Movement.Interfaces;
using UnityEngine;

namespace Game.Movement.Controllers
{
    public class CharacterMovementStorageImp : CharacterMovementStorage
    {
        private readonly Dictionary<ICharacter, ICharacterMovement> _characterMovements = new ();

        public override ICharacterMovement this[ICharacter character] => _characterMovements.ContainsKey(character)
            ? _characterMovements[character]
            : throw new NullReferenceException("Character movement not found " + character.CharacterID);

        public override void AddCharacterMovements(ICharacter character, ICharacterMovement characterMovement)
        {
            if (_characterMovements.ContainsKey(character)) return;
            _characterMovements.Add(character, characterMovement);
        }

        public override void RemoveCharacterMovements(ICharacter character)
        {
            if (!_characterMovements.ContainsKey(character)) return;
            _characterMovements.Remove(character);
        }
    }
}