using System;
using System.Collections.Generic;
using Common.InputHandlers.Enums;
using Common.InputHandlers.Interfaces;
using Game.Movement.Interfaces;
using UnityEngine;

namespace Game.Movement.Controllers
{
    public class PlayerDirection : IPlayerDirection, IDisposable
    {
        private readonly IInputHandler _inputHandler;
        private readonly Dictionary<EInputButton, Vector2> _movementDirections = new()
        {
            [EInputButton.Down] = Vector2.down,
            [EInputButton.Up] = Vector2.up,
            [EInputButton.Left] = Vector2.left,
            [EInputButton.Right] = Vector2.right
        };
        
        public event Action<Vector2> DirectionChange;
        public Vector2 Direction { get; private set; }

        public PlayerDirection(IInputHandler inputHandler)
        {
            _inputHandler = inputHandler;
            _inputHandler.InputDown += AddDirection;
            _inputHandler.InputUp += RemoveDirection;
        }
        
        private void AddDirection(EInputButton button)
        {
            if (!_movementDirections.ContainsKey(button)) return;
            Direction += _movementDirections[button];
            DirectionChange?.Invoke(Direction);
        }
        
        private void RemoveDirection(EInputButton button)
        {
            if (!_movementDirections.ContainsKey(button)) return;
            Direction -= _movementDirections[button];
            DirectionChange?.Invoke(Direction);
        }

        public void Dispose()
        {
            DirectionChange = null;
            _inputHandler.InputDown -= AddDirection;
            _inputHandler.InputUp -= RemoveDirection;
        }
    }
}