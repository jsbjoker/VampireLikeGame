using Common.Mediators.Interfaces;
using Game.Characters.Interfaces;
using Game.Characters.Views;
using Game.Movement.Interfaces;
using UnityEngine;

namespace Game.Movement.Controllers
{
    public class CharacterMovementFactoryImp : CharacterMovementFactory
    {
        [SerializeField] private CharacterMovementStorage _movementStorage;
        [SerializeField] private MediatorFacade _mediatorFacade;
        
        public override ICharacterMovement CreateCharacterMovement(ICharacter character, CharacterView characterView)
        {
            var movement = new CharacterPhysicMovement(characterView.transform, characterView.GetComponent<Rigidbody2D>(), character.Model);
            _movementStorage.AddCharacterMovements(character, movement);
            return movement;
        }
    }
}