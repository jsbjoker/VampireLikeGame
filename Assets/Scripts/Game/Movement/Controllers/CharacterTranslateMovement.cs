using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Movement.Interfaces;
using UnityEngine;

namespace Game.Movement.Controllers
{
    public class CharacterTranslateMovement : ICharacterMovement
    {
        private readonly Transform _characterTransform;
        private readonly ICharacterModel _characterModel;
        private Vector2 _direction;

        public bool RightFacing { get; private set; }

        public void Stop() {}
        public bool IsMoving => _direction != Vector2.zero;

        public CharacterTranslateMovement(Transform characterTransform, ICharacterModel characterModel)
        {
            _characterTransform = characterTransform;
            _characterModel = characterModel;
            RightFacing = true;
        }
        
        public void MoveTo(Vector2 direction) => _direction = direction;
        
        public void FixedUpdate(float deltaTime)
        {
            if (_direction == Vector2.zero) return;
            _characterTransform.Translate(_direction * _characterModel[ECharacterParam.MoveSpeed] * deltaTime);
            
            if (_direction.x < 0f && RightFacing)
               FlipCharacter();

            if (_direction.x > 0f && !RightFacing)
                FlipCharacter();
        }

        private void FlipCharacter()
        {
            RightFacing = !RightFacing;
            var scale = _characterTransform.localScale;
            scale.x *= -1;
            _characterTransform.localScale = scale;
        }
    }
}