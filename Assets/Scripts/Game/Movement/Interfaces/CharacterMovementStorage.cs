using Game.Characters.Interfaces;
using UnityEngine;

namespace Game.Movement.Interfaces
{
    public abstract class CharacterMovementStorage : MonoBehaviour
    {
        public abstract ICharacterMovement this[ICharacter character] { get; }
        public abstract void AddCharacterMovements(ICharacter character, ICharacterMovement characterMovement);
        public abstract void RemoveCharacterMovements(ICharacter character);
    }
}