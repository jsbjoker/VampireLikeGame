using UnityEngine;

namespace Game.Movement.Interfaces
{
    public abstract class CharacterMovementFacade : MonoBehaviour
    {
        public abstract CharacterMovementFactory CharacterMovementFactory { get; }
        public abstract CharacterMovementStorage CharacterMovementStorage { get; }
    }
}