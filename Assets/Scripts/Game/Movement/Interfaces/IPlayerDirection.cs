using System;
using UnityEngine;

namespace Game.Movement.Interfaces
{
    public interface IPlayerDirection
    {
        event Action<Vector2> DirectionChange;
        Vector2 Direction { get; }
    }
}