using UnityEngine;

namespace Game.Movement.Interfaces
{
    public interface ICharacterMovement
    {
        bool RightFacing { get; }
        void MoveTo(Vector2 direction);
        void Stop();
        bool IsMoving { get; }
    }
}