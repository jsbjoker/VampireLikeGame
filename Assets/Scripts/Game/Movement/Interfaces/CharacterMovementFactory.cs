using Game.Characters.Interfaces;
using Game.Characters.Views;
using UnityEngine;

namespace Game.Movement.Interfaces
{
    public abstract class CharacterMovementFactory : MonoBehaviour
    {
        public abstract ICharacterMovement CreateCharacterMovement(ICharacter character, CharacterView characterView);
    }
}