using Game.Inventory.Enums;

namespace Game.Inventory.Interfaces
{
    public interface IItem
    {
        EItem ItemType { get; }
    }
}