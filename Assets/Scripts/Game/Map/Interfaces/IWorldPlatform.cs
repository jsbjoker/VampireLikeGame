using Game.Map.Enums;
using UnityEngine;

namespace Game.Map.Interfaces
{
    public interface IWorldPlatform
    {
        Vector3 Position { get; }
        EPlatformLine PlatformLine { get; }
        void SetPlatformLine(EPlatformLine line);
        void SetPlatformPosition(Vector3 position);
    }
}