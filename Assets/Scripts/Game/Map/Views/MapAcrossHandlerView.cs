using System;
using Game.Map.Interfaces;
using UnityEngine;

namespace Game.Map.Views
{
    public class MapAcrossHandlerView : MonoBehaviour
    {
        private Collider2D _lastPlatform;
        
        public event Action<IWorldPlatform> PlatformAcross;

        public Vector3 Position => transform.position;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (_lastPlatform == other) return;
            var worldPlatform = other.GetComponent<IWorldPlatform>();
            if (worldPlatform == null) return;
            _lastPlatform = other;
            PlatformAcross?.Invoke(worldPlatform);
        }

        private void OnDestroy()
        {
            PlatformAcross = null;
        }
    }
}