using Game.Map.Enums;
using Game.Map.Interfaces;
using UnityEngine;

namespace Game.Map.Views
{
    public class WorldPlatformView : MonoBehaviour, IWorldPlatform
    {
        [SerializeField] private EPlatformLine _platformLine;
        public Vector3 Position => transform.position;
        public EPlatformLine PlatformLine => _platformLine;
        
        
        public void SetPlatformLine(EPlatformLine line)
        {
            if (PlatformLine == line) return;
            _platformLine = line;
            SetPlatformName();
        }

        public void SetPlatformPosition(Vector3 position)
        {
            transform.position = position;
        }

        public void SetPlatformName()
        {
            gameObject.name = "Tilemap - base " + _platformLine;
        }
    }
}