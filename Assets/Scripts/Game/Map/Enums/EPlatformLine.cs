namespace Game.Map.Enums
{
    public enum EPlatformLine
    {
        None,
        X,
        Y,
        XY
    }
}