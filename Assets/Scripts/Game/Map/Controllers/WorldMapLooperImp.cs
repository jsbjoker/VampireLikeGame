using System;
using System.Collections.Generic;
using Game.Map.Enums;
using Game.Map.Interfaces;
using Game.Map.Views;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Game.Map.Controllers
{
    public class WorldMapLooperImp : WorldMapLooper
    {
        [SerializeField] private List<WorldPlatformView> _platformViews;
        [SerializeField] private Tilemap _tilemap;
        [SerializeField] private MapAcrossHandlerView _acrossHandler;
        private Dictionary<EPlatformLine, IWorldPlatform> _worldPlatforms;
        private IWorldPlatform _currentPlatform;
        private Vector3Int _mapSize;
        private bool _xLastSideRight;
        private bool _yLastSideTop;
        private bool _xyReasonToChange;
        private Vector3 _xyTargetPosition = Vector3.zero;

        private void Awake()
        {
            _mapSize = _tilemap.size;
            _worldPlatforms = new Dictionary<EPlatformLine, IWorldPlatform>();
            foreach (var platformView in _platformViews)
            {
                platformView.SetPlatformName();
                _worldPlatforms.Add(platformView.PlatformLine, platformView);
                if (platformView.PlatformLine == EPlatformLine.None)
                    _currentPlatform = platformView;
            }
            
            _xLastSideRight = _acrossHandler.Position.x > _currentPlatform.Position.x;
            SetupXPlatform(_xLastSideRight);
            _yLastSideTop = _acrossHandler.Position.y > _currentPlatform.Position.y;
            SetupYPlatform(_yLastSideTop);
            SetupXYPlatform();
            _acrossHandler.PlatformAcross += HandleAcrossPlatform;
        }

        private void HandleAcrossPlatform(IWorldPlatform newPlatform)
        {
            if (_currentPlatform == newPlatform) return;
            SwapPlatforms(newPlatform.PlatformLine);
            _currentPlatform.SetPlatformLine(newPlatform.PlatformLine);
            _worldPlatforms[newPlatform.PlatformLine] = _currentPlatform;
            _currentPlatform = newPlatform;
            _currentPlatform.SetPlatformLine(EPlatformLine.None);
        }

        private void SwapPlatforms(EPlatformLine line)
        {
            switch (line)
            {
                case EPlatformLine.X:
                    _worldPlatforms[EPlatformLine.XY].SetPlatformLine(EPlatformLine.Y);
                    _worldPlatforms[EPlatformLine.Y].SetPlatformLine(EPlatformLine.XY);
                    var y = _worldPlatforms[EPlatformLine.Y];
                    _worldPlatforms[EPlatformLine.Y] = _worldPlatforms[EPlatformLine.XY];
                    _worldPlatforms[EPlatformLine.XY] = y;
                    break;
                case EPlatformLine.Y:
                    _worldPlatforms[EPlatformLine.X].SetPlatformLine(EPlatformLine.XY);
                    _worldPlatforms[EPlatformLine.XY].SetPlatformLine(EPlatformLine.X);
                    var x = _worldPlatforms[EPlatformLine.X];
                    _worldPlatforms[EPlatformLine.X] = _worldPlatforms[EPlatformLine.XY];
                    _worldPlatforms[EPlatformLine.XY] = x;
                    break;
                case EPlatformLine.XY:
                    _worldPlatforms[EPlatformLine.X].SetPlatformLine(EPlatformLine.Y);
                    _worldPlatforms[EPlatformLine.Y].SetPlatformLine(EPlatformLine.X);
                    var xy = _worldPlatforms[EPlatformLine.X];
                    _worldPlatforms[EPlatformLine.X] = _worldPlatforms[EPlatformLine.Y];
                    _worldPlatforms[EPlatformLine.Y] = xy;
                    break;
            }
        }

        private void Update()
        {
            var rightSide = _acrossHandler.Position.x > _currentPlatform.Position.x;
            if (rightSide != _xLastSideRight)
                SetupXPlatform(rightSide);
            
            var topSide = _acrossHandler.Position.y > _currentPlatform.Position.y;
            if (topSide != _yLastSideTop)
                SetupYPlatform(topSide);

            if (_xyReasonToChange)
                SetupXYPlatform();
        }

        private void SetupXPlatform(bool rightSide)
        {
            var targetPlatformPosition = new Vector3(rightSide
                ? _currentPlatform.Position.x + _mapSize.x
                : _currentPlatform.Position.x - _mapSize.x, _currentPlatform.Position.y);
            _worldPlatforms[EPlatformLine.X].SetPlatformPosition(targetPlatformPosition);
            _xyTargetPosition.x = targetPlatformPosition.x;
            _xLastSideRight = rightSide;
            _xyReasonToChange = true;
        }
        
        private void SetupYPlatform(bool topSide)
        {
            var targetPlatformPosition = new Vector3(_currentPlatform.Position.x, topSide
                ? _currentPlatform.Position.y + _mapSize.y
                : _currentPlatform.Position.y - _mapSize.y);
            _worldPlatforms[EPlatformLine.Y].SetPlatformPosition(targetPlatformPosition);
            _xyTargetPosition.y = targetPlatformPosition.y;
            _yLastSideTop = topSide;
            _xyReasonToChange = true;
        }

        private void SetupXYPlatform()
        {
            _worldPlatforms[EPlatformLine.XY].SetPlatformPosition(_xyTargetPosition);
            _xyReasonToChange = false;
        }
        
        private void OnDestroy()
        {
            _acrossHandler.PlatformAcross -= HandleAcrossPlatform;
        }
    }
}