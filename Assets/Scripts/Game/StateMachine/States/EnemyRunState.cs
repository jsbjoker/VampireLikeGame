using Game.Animation.Enums;
using Game.Animation.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Movement.Interfaces;
using Game.StateMachine.Enums;
using Game.StateMachine.Interfaces;
using UnityEngine;

namespace Game.StateMachine.States
{
    public class EnemyRunState : CharacterState
    {
        private readonly ICharacter _player;
        private readonly ICharacterAnimator _characterAnimator;
        private readonly ICharacterMovement _characterMovement;
        private readonly Transform _playerTransform;
        private readonly Transform _enemyTransform;
        
        public override ECharacterState State => ECharacterState.Run;
        
        public EnemyRunState(ICharacterStateMachine stateMachine, ICharacter character, ICharacter player,
            CharacterTransformStorage characterTransformStorage, ICharacterAnimator characterAnimator,
            ICharacterMovement characterMovement)
            : base(stateMachine, character)
        {
            _player = player;
            _playerTransform = characterTransformStorage.GetTransformByCharacter(player);
            _enemyTransform = characterTransformStorage.GetTransformByCharacter(character);
            _characterAnimator = characterAnimator;
            _characterMovement = characterMovement;
            Character.CollisionEnter += OnCollisionEnter;
        }

        private void OnCollisionEnter(Transform transform)
        {
            if (transform != _playerTransform) return;
            StateMachine.AdvanceState(ECharacterState.Attack);
        }

        public override void Start()
        {
            _characterAnimator.SetBool(EAnimationType.Run, true);
        }

        public override void Update(float deltaTime)
        {
            if (_player == null || !_player.Model.IsAlive)
            {
                StateMachine.AdvanceState(ECharacterState.Idle);
                return;
            }
        }

        public override void FixedUpdate(float deltaTime)
        {
            if (_player == null || !_player.Model.IsAlive) return;
            if (Vector3.Distance(_enemyTransform.position, _playerTransform.position) < _player.Model[ECharacterParam.AttackRange]) return;
            var direction = (_playerTransform.position - _enemyTransform.position).normalized;
            _characterMovement.MoveTo(direction); 
        }

        public override void Exit()
        {
            _characterMovement.Stop();
        }

        public override void Dispose()
        {
            base.Dispose();
            Character.CollisionEnter -= OnCollisionEnter;
        }
    }
}