using Game.Animation.Enums;
using Game.Animation.Interfaces;
using Game.Characters.Interfaces;
using Game.Movement.Interfaces;
using Game.StateMachine.Enums;
using Game.StateMachine.Interfaces;
using UnityEngine;

namespace Game.StateMachine.States
{
    public class PlayerRunState : CharacterState
    {
        private readonly IPlayerDirection _playerDirection;
        private readonly ICharacterAnimator _characterAnimator;
        private readonly ICharacterMovement _characterMovement;
        public override ECharacterState State => ECharacterState.Run;

        public PlayerRunState(ICharacterStateMachine stateMachine, ICharacter character,
            IPlayerDirection playerDirection, ICharacterAnimator characterAnimator,
            ICharacterMovement characterMovement) : base(stateMachine, character)
        {
            _playerDirection = playerDirection;
            _characterAnimator = characterAnimator;
            _characterMovement = characterMovement;
        }

        public override void Start()
        {
            _characterAnimator.SetSpeed(1f);
            _characterAnimator.SetBool(EAnimationType.Run, true);
        }
        
        public override void Update(float deltaTime)
        {
            if (StateMachine.CurrentState != State) return; 
            if (_playerDirection.Direction != Vector2.zero) return;
            StateMachine.AdvanceState(ECharacterState.Idle);
        }

        public override void FixedUpdate(float deltaTime)
        {
            if (StateMachine.CurrentState != State) return;
            _characterMovement.MoveTo(_playerDirection.Direction);
        }

        public override void Exit()
        {
            _characterMovement.Stop();
        }
    }
}