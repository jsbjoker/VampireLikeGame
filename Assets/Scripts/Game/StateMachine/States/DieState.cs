using Game.Animation.Enums;
using Game.Animation.Interfaces;
using Game.Characters.Interfaces;
using Game.Movement.Interfaces;
using Game.StateMachine.Enums;
using Game.StateMachine.Interfaces;
using UnityEngine;

namespace Game.StateMachine.States
{
    public class DieState : CharacterState
    {
        private readonly IDeadly _deadlyCharacter;
        private readonly ICharacterAnimator _characterAnimator;
        private readonly ICharacterMovement _characterMovement;
        public override ECharacterState State => ECharacterState.Die;

        public DieState(ICharacterStateMachine stateMachine, IDeadly character,
            ICharacterAnimator characterAnimator, ICharacterMovement characterMovement)
            : base(stateMachine, character)
        {
            _deadlyCharacter = character;
            _characterAnimator = characterAnimator;
            _characterMovement = characterMovement;
        }

        public override void Start()
        {
            _characterAnimator.SetSpeed(1f);
            _characterMovement.MoveTo(Vector2.zero);
            _deadlyCharacter.SetActiveCollider(false);
            _characterAnimator.SetTrigger(EAnimationType.Die, () =>
            {
                _deadlyCharacter.Die();
            });
        }
    }
}