using Game.Animation.Enums;
using Game.Animation.Interfaces;
using Game.Characters.Interfaces;
using Game.Movement.Interfaces;
using Game.StateMachine.Enums;
using Game.StateMachine.Interfaces;
using UnityEngine;

namespace Game.StateMachine.States
{
    public class PlayerIdleState : CharacterState
    {
        private readonly ICharacterAnimator _characterAnimator;
        private readonly IPlayerDirection _playerDirection;
        public override ECharacterState State => ECharacterState.Idle;
        private Vector2 _direction;

        public PlayerIdleState(ICharacterStateMachine stateMachine, ICharacter character,
            ICharacterAnimator characterAnimator, IPlayerDirection playerDirection) : base(stateMachine, character)
        {
            _characterAnimator = characterAnimator;
            _playerDirection = playerDirection;
        }
        
        public override void Start()
        {
            _characterAnimator.SetSpeed(1f);
            _characterAnimator.SetBool(EAnimationType.Run, false);
        }

        public override void Update(float deltaTime)
        {
            if (StateMachine.CurrentState != State) return;
            if (_playerDirection.Direction == Vector2.zero) return;
            StateMachine.AdvanceState(ECharacterState.Run);
        }
    }
}