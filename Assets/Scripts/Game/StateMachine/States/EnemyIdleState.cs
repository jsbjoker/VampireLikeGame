using Game.Animation.Enums;
using Game.Animation.Interfaces;
using Game.Characters.Interfaces;
using Game.Movement.Interfaces;
using Game.StateMachine.Enums;
using Game.StateMachine.Interfaces;
using UnityEngine;

namespace Game.StateMachine.States
{
    public class EnemyIdleState : CharacterState
    {
        private readonly ICharacter _player;
        private readonly ICharacterAnimator _characterAnimator;
        private readonly ICharacterMovement _characterMovement;
        public override ECharacterState State => ECharacterState.Idle;
        
        public EnemyIdleState(ICharacterStateMachine stateMachine, ICharacter character, ICharacter player,
            ICharacterAnimator characterAnimator, ICharacterMovement characterMovement) 
            : base(stateMachine, character)
        {
            _player = player;
            _characterAnimator = characterAnimator;
            _characterMovement = characterMovement;
        }

        public override void Start()
        {
            _characterMovement.MoveTo(Vector2.zero);
            _characterAnimator.SetBool(EAnimationType.Run, false);
        }

        public override void Update(float deltaTime)
        {
            if (_player == null) return; 
            if (!_player.Model.IsAlive) return;
            StateMachine.AdvanceState(ECharacterState.Run);
        }
    }
}