using Game.Animation.Enums;
using Game.Animation.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Damage.Interfaces;
using Game.Movement.Interfaces;
using Game.StateMachine.Enums;
using Game.StateMachine.Interfaces;
using UnityEngine;

namespace Game.StateMachine.States
{
    public class EnemyAttackState : CharacterState
    {
        private readonly ICharacter _player;
        private readonly ICharacterAnimator _animator;
        private readonly IDamageModel _damageModel;
        private readonly ICharacterMovement _movement;
        private readonly Transform _playerTransform; 
        public override ECharacterState State => ECharacterState.Attack;
        
        public EnemyAttackState(ICharacterStateMachine stateMachine, ICharacter character, ICharacter player, ICharacterAnimator animator,
            CharacterTransformStorage characterTransformStorage, IDamageModel damageModel, ICharacterMovement movement) 
            : base(stateMachine, character)
        {
            _player = player;
            _animator = animator;
            _damageModel = damageModel;
            _movement = movement;
            _playerTransform = characterTransformStorage.GetTransformByCharacter(player);
            Character.CollisionExit += OnCollisionExit;
        }

        private void OnCollisionExit(Transform transform)
        {
            if (transform != _playerTransform) return;
            StateMachine.AdvanceState(ECharacterState.Run);
        }

        public override void Start()
        {
            _animator.SetBool(EAnimationType.Run, false);
            _movement.MoveTo(Vector2.zero);
        }
        
        public override void Update(float deltaTime)
        {
            _damageModel.DealDamage(Character.Model[ECharacterParam.Damage] * Time.deltaTime, _player);
        }

        public override void Dispose()
        {
            base.Dispose();
            Character.CollisionExit -= OnCollisionExit;
        }
    }
}