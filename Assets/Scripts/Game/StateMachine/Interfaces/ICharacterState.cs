using System;
using Common.Mediators.Interfaces;
using Game.StateMachine.Enums;

namespace Game.StateMachine.Interfaces
{
    public interface ICharacterState : IFixedUpdatable, IUpdatable, IDisposable
    {
        ECharacterState State { get; }
        void Start();
        void Exit();
    }
}