using UnityEngine;

namespace Game.StateMachine.Interfaces
{
    public abstract class CharacterStateMachineFacade : MonoBehaviour
    {
        public abstract CharacterStateMachineFactory CharacterStateMachineFactory { get; }
        public abstract CharacterStateMachineStorage CharacterStateMachineStorage { get; }
    }
}