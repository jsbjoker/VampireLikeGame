using System;
using Common.Mediators.Interfaces;
using Game.StateMachine.Enums;

namespace Game.StateMachine.Interfaces
{
    public interface ICharacterStateMachine : IFixedUpdatable, IUpdatable, IDisposable
    {
        ECharacterState CurrentState { get; }
        void AdvanceState(ECharacterState state);
    }
}