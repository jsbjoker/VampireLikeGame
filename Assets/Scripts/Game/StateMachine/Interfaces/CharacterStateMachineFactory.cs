using Game.Characters.Interfaces;
using UnityEngine;

namespace Game.StateMachine.Interfaces
{
    public abstract class CharacterStateMachineFactory : MonoBehaviour
    {
        public abstract ICharacterStateMachine CreateCharacterStateMachine(IDeadly character);
    }
}