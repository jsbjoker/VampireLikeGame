using Game.Characters.Interfaces;
using UnityEngine;

namespace Game.StateMachine.Interfaces
{
    public abstract class CharacterStateMachineStorage : MonoBehaviour
    {
        public abstract ICharacterStateMachine this[ICharacter character] { get; }

        public abstract void AddCharacterStateMachine(ICharacter character,
            ICharacterStateMachine characterStateMachine);

        public abstract void RemoveCharacterStateMachine(ICharacter character);
    }
}