using Game.Characters.Interfaces;
using Game.StateMachine.Enums;

namespace Game.StateMachine.Interfaces
{
    public abstract class CharacterState : ICharacterState
    {
        protected readonly ICharacterStateMachine StateMachine;
        protected readonly ICharacter Character;
        
        protected CharacterState(ICharacterStateMachine stateMachine, ICharacter character)
        {
            StateMachine = stateMachine;
            Character = character;
        }

        public abstract ECharacterState State { get; }
        public virtual void Start() {}
        public virtual void Exit() {}
        public virtual void Update(float deltaTime) {}
        public virtual void Dispose() {}
        public virtual void FixedUpdate(float deltaTime) {}
    }
}