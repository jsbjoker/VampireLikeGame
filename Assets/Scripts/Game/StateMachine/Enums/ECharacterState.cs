namespace Game.StateMachine.Enums
{
    public enum ECharacterState
    {
        Idle,
        Run,
        Attack,
        Die
    }
}