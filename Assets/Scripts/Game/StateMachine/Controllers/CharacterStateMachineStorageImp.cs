using System;
using System.Collections.Generic;
using Common.Mediators.Interfaces;
using Game.Characters.Interfaces;
using Game.StateMachine.Interfaces;
using UnityEngine;

namespace Game.StateMachine.Controllers
{
    public class CharacterStateMachineStorageImp : CharacterStateMachineStorage
    {
        [SerializeField] private MediatorFacade _mediatorFacade;
        private readonly Dictionary<ICharacter, ICharacterStateMachine> _characterStateMachines = new();

        public override ICharacterStateMachine this[ICharacter character] =>
            _characterStateMachines.ContainsKey(character)
                ? _characterStateMachines[character]
                : throw new NullReferenceException("Character state machine not found " + character.CharacterID);

        public override void AddCharacterStateMachine(ICharacter character, ICharacterStateMachine characterStateMachine)
        {
            if (_characterStateMachines.ContainsKey(character)) return;
            _characterStateMachines.Add(character, characterStateMachine);
        }

        public override void RemoveCharacterStateMachine(ICharacter character)
        {
            if (!_characterStateMachines.ContainsKey(character)) return;
            _mediatorFacade.UpdateMediator.RemoveUpdatable(_characterStateMachines[character]);
            _mediatorFacade.FixedUpdateMediator.RemoveFixedUpdatable(_characterStateMachines[character]);
            _characterStateMachines[character]?.Dispose();
            _characterStateMachines.Remove(character);
        }
    }
}