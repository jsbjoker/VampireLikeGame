using Common.InputHandlers.Interfaces;
using Common.Mediators.Interfaces;
using Game.Animation.Interfaces;
using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Damage.Interfaces;
using Game.Movement.Controllers;
using Game.Movement.Interfaces;
using Game.StateMachine.Enums;
using Game.StateMachine.Interfaces;
using Game.StateMachine.States;
using UnityEngine;

namespace Game.StateMachine.Controllers
{
    public class CharacterStateMachineFactoryImp : CharacterStateMachineFactory
    {
        [SerializeField] private CharacterStateMachineStorage _characterStateMachineStorage;
        [SerializeField] private CharacterAnimatorStorage _characterAnimatorStorage;
        [SerializeField] private CharacterMovementStorage _characterMovementStorage;
        [SerializeField] private CharacterFacade _characterFacade;
        [SerializeField] private InputHandler _inputHandler;
        [SerializeField] private MediatorFacade _mediatorFacade;
        [SerializeField] private DamageModel _damageModel;
        
        public override ICharacterStateMachine CreateCharacterStateMachine(IDeadly character)
        {
            var stateMachine = character.Team == ETeam.Player ? CreatePlayerStateMachine(character) : CreateEnemyStateMachine(character);
            _characterStateMachineStorage.AddCharacterStateMachine(character, stateMachine);
            return stateMachine;
        }

        private ICharacterStateMachine CreatePlayerStateMachine(IDeadly character)
        {
            var playerStateMachine = new CharacterStateMachine();
            var characterAnimator = _characterAnimatorStorage[character];
            var playerDirection = new PlayerDirection(_inputHandler);
            var idleState = new PlayerIdleState(playerStateMachine, character, characterAnimator, playerDirection);
            playerStateMachine.AddState(idleState);
            var playerMovement = _characterMovementStorage[character];
            var runState = new PlayerRunState(playerStateMachine, character, playerDirection, characterAnimator,
                playerMovement);
            playerStateMachine.AddState(runState);
            var dieState = new DieState(playerStateMachine, character, characterAnimator, playerMovement);
            playerStateMachine.AddState(dieState);
            playerStateMachine.AdvanceState(ECharacterState.Idle);
            _mediatorFacade.UpdateMediator.AddUpdatable(playerStateMachine);
            _mediatorFacade.FixedUpdateMediator.AddFixedUpdatable(playerStateMachine);
            return playerStateMachine;
        }
        
        private ICharacterStateMachine CreateEnemyStateMachine(IDeadly character)
        {
            var enemyStateMachine = new CharacterStateMachine();
            var player = _characterFacade.CharacterStorage.Player;
            var characterTransformStorage = _characterFacade.CharacterTransformStorage;
            var enemyAnimator = _characterAnimatorStorage[character];
            var enemyMovement = _characterMovementStorage[character];
            var idleState = new EnemyIdleState(enemyStateMachine, character, player,
                enemyAnimator, enemyMovement);
            enemyStateMachine.AddState(idleState);
            var runState = new EnemyRunState(enemyStateMachine, character, player,
                characterTransformStorage, enemyAnimator, enemyMovement);
            enemyStateMachine.AddState(runState);
            var dieState = new DieState(enemyStateMachine, character, enemyAnimator, enemyMovement);
            enemyStateMachine.AddState(dieState);
            var attackState = new EnemyAttackState(enemyStateMachine, character, player, enemyAnimator,
                characterTransformStorage, _damageModel, enemyMovement);
            enemyStateMachine.AddState(attackState);
            enemyStateMachine.AdvanceState(ECharacterState.Idle);
            _mediatorFacade.UpdateMediator.AddUpdatable(enemyStateMachine);
            _mediatorFacade.FixedUpdateMediator.AddFixedUpdatable(enemyStateMachine);
            return enemyStateMachine;
        }
    }
}