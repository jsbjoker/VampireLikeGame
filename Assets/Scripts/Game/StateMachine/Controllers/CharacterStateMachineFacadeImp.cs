using Game.StateMachine.Interfaces;
using UnityEngine;

namespace Game.StateMachine.Controllers
{
    public class CharacterStateMachineFacadeImp : CharacterStateMachineFacade
    {
        [SerializeField] private CharacterStateMachineFactory _characterStateMachineFactory;
        [SerializeField] private CharacterStateMachineStorage _characterStateMachineStorage;


        public override CharacterStateMachineFactory CharacterStateMachineFactory => _characterStateMachineFactory;
        public override CharacterStateMachineStorage CharacterStateMachineStorage => _characterStateMachineStorage;
    }
}