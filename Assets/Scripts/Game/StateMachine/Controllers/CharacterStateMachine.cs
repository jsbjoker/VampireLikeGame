using System.Collections.Generic;
using Game.StateMachine.Enums;
using Game.StateMachine.Interfaces;
using UnityEngine;

namespace Game.StateMachine.Controllers
{
    public class CharacterStateMachine : ICharacterStateMachine
    {
        private readonly Dictionary<ECharacterState, ICharacterState> _states = new ();
        private ICharacterState _currentState;
        public ECharacterState CurrentState => _currentState.State;
        
        public void AddState(ICharacterState characterState)
        {
            if (_states.ContainsKey(characterState.State)) return;
            _states.Add(characterState.State, characterState);
        }
        
        public void AdvanceState(ECharacterState state)
        {
            if (!_states.ContainsKey(state)) return;
            if (_currentState != null && _currentState.State == state) return;
            _currentState?.Exit();
            _currentState = _states[state];
            _currentState?.Start();
        }

        public void Update(float deltaTime)
        {
            _currentState?.Update(deltaTime);
        }
        
        public void FixedUpdate(float deltaTime)
        {
            _currentState?.FixedUpdate(deltaTime);
        }

        public void Dispose()
        {
            foreach (var state in _states)
                state.Value.Dispose();
        }
    }
}