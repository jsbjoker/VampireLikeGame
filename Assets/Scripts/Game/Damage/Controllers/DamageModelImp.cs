using Game.Characters.Enums;
using Game.Characters.Interfaces;
using Game.Damage.Interfaces;
using Game.StateMachine.Enums;
using Game.StateMachine.Interfaces;
using UnityEngine;

namespace Game.Damage.Controllers
{
    public class DamageModelImp : DamageModel
    {
        [SerializeField] private CharacterStateMachineStorage _stateMachineStorage;

        public override void DealDamage(float damage, ICharacter to)
        {
            if (to.Model[ECharacterParam.Health] - damage > 0f)
            {
                var resultHealth = to.Model[ECharacterParam.Health] - damage;
                to.Model.SetCharacterParam(ECharacterParam.Health, resultHealth);
            }
            else
            {
                // var stateMachine = _stateMachineStorage[to];
                // to.Model.SetCharacterParam(ECharacterParam.Health, 0f);
                // stateMachine.AdvanceState(ECharacterState.Die);
            }
        }
    }
}