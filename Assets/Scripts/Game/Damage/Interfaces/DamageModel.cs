using Game.Characters.Interfaces;
using UnityEngine;

namespace Game.Damage.Interfaces
{
    public abstract class DamageModel : MonoBehaviour, IDamageModel
    {
        public abstract void DealDamage(float damage, ICharacter to);
    }
}