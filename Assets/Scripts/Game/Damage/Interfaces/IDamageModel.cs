using Game.Characters.Interfaces;

namespace Game.Damage.Interfaces
{
    public interface IDamageModel
    {
        void DealDamage(float damage, ICharacter to);
    }
}