using Common.Config.Enums;
using UnityEngine;

namespace Common.Config.Interfaces
{
    public abstract class ConfigProvider : MonoBehaviour
    {
        public abstract T GetConfig<T>(EConfig configType);
    }
}