using System;
using Common.Config.Enums;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace Common.Config.Settings
{
    [CreateAssetMenu(fileName = "ConfigSettings", menuName = "Settings/Common/ConfigSettings")]
    public class ConfigSettings : ScriptableObject
    {
        [SerializeField] private ConfigDictionary _configs;
        [SerializeField] private ConfigDictionary _overrideConfigs;

        public TextAsset GetConfigText(EConfig config)
        {
            if (_overrideConfigs.ContainsKey(config))
                return _overrideConfigs[config];

            return _configs.ContainsKey(config)
                ? _configs[config]
                : throw new NullReferenceException("Config not found " + config);
        }
    }

    [Serializable]
    public class ConfigDictionary : SerializableDictionaryBase<EConfig, TextAsset>
    {
    }
}