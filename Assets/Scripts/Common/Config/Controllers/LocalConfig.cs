using Common.Config.Enums;
using Common.Config.Interfaces;
using Common.Config.Settings;
using Common.TextSerialization.Interfaces;
using UnityEngine;

namespace Common.Config.Controllers
{
    public class LocalConfig : ConfigProvider
    {
        [SerializeField] private ConfigSettings _configSettings;
        [SerializeField] private TextSerializer _textSerializer;

        public override T GetConfig<T>(EConfig config)
        {
            var defaultConfigText = _configSettings.GetConfigText(config);
            if (defaultConfigText != null && !string.IsNullOrEmpty(defaultConfigText.text))
                if (!_textSerializer.Deserialize<T>(defaultConfigText.text, out var deserializeConfig))
                    return deserializeConfig;

            return default;
        }
    }
}