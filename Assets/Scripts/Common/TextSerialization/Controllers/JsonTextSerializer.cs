using Common.TextSerialization.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Common.TextSerialization.Controllers
{
    public class JsonTextSerializer : TextSerializer
    {
        private void Awake()
        {
            JsonConvert.DefaultSettings = () =>
            {
                var settings = new JsonSerializerSettings();
                settings.Converters.Add(new StringEnumConverter { CamelCaseText = false });
                return settings;
            };
        }

        public override bool Serialize(object obj, out string serializeData)
        {
            var hasError = false;
            serializeData = JsonConvert.SerializeObject(obj, new JsonSerializerSettings
            {
                Error = delegate { hasError = true; }
            });
            return hasError;
        }

        public override bool Deserialize<T>(string text, out T config)
        {
            var hasErrors = false;
            config = JsonConvert.DeserializeObject<T>(text, new JsonSerializerSettings
            {
                Error = delegate { hasErrors = true; }
            });
            return hasErrors;
        }
    }
}