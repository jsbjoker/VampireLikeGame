using System.Collections.Generic;
using System.Linq;
using Common.Mediators.Interfaces;
using UnityEngine;

namespace Common.Mediators.Controllers
{
    public class UpdateMediatorImp : UpdateMediator
    {
        private List<IUpdatable> _updatables = new ();

        public override void AddUpdatable(IUpdatable updatable)
        {
            _updatables.Add(updatable);
        }

        public override void RemoveUpdatable(IUpdatable updatable)
        {
            _updatables.Remove(updatable);
        }

        private void Update()
        {
            foreach (var updatable in _updatables.ToList())
                updatable.Update(Time.deltaTime);
        }
    }
}