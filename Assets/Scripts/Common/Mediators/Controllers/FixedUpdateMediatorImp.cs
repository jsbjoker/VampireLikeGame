using System.Collections.Generic;
using System.Linq;
using Common.Mediators.Interfaces;
using UnityEngine;

namespace Common.Mediators.Controllers
{
    public class FixedUpdateMediatorImp : FixedUpdateMediator
    {
        private List<IFixedUpdatable> _fixedUpdatables = new ();


        public override void AddFixedUpdatable(IFixedUpdatable fixedUpdatable)
        {
            _fixedUpdatables.Add(fixedUpdatable);
        }

        public override void RemoveFixedUpdatable(IFixedUpdatable fixedUpdatable)
        {
            _fixedUpdatables.Remove(fixedUpdatable);
        }

        private void FixedUpdate()
        {
            foreach (var fixedUpdatable in _fixedUpdatables.ToList())
                fixedUpdatable.FixedUpdate(Time.deltaTime);
        }
    }
}