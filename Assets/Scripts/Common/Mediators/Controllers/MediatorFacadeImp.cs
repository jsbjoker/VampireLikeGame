using Common.Mediators.Interfaces;
using UnityEngine;

namespace Common.Mediators.Controllers
{
    public class MediatorFacadeImp : MediatorFacade
    {
        [SerializeField] private FixedUpdateMediator _fixedUpdateMediator;
        [SerializeField] private UpdateMediator _updateMediator;
        [SerializeField] private DisposableMediator _disposableMediator;


        public override FixedUpdateMediator FixedUpdateMediator => _fixedUpdateMediator;
        public override UpdateMediator UpdateMediator => _updateMediator;
        public override DisposableMediator DisposableMediator => _disposableMediator;
    }
}