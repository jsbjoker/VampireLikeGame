using System;
using System.Collections.Generic;
using Common.Mediators.Interfaces;

namespace Common.Mediators.Controllers
{
    public class DisposableMediatorImp : DisposableMediator
    {
        private List<IDisposable> _disposables = new ();

        public override void AddDisposable(IDisposable disposable)
        {
            _disposables.Add(disposable);
        }

        private void OnDestroy()
        {
            foreach (var disposable in _disposables)
            {
                disposable.Dispose();
            }
        }
    }
}