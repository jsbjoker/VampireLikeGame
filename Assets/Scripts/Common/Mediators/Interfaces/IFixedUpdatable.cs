namespace Common.Mediators.Interfaces
{
    public interface IFixedUpdatable
    {
        void FixedUpdate(float deltaTime);
    }
}