namespace Common.Mediators.Interfaces
{
    public interface IUpdatable
    {
        void Update(float deltaTime);
    }
}