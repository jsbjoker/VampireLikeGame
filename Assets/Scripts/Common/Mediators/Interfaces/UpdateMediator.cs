using UnityEngine;

namespace Common.Mediators.Interfaces
{
    public abstract class UpdateMediator : MonoBehaviour
    {
        public abstract void AddUpdatable(IUpdatable updatable);
        public abstract void RemoveUpdatable(IUpdatable updatable);
    }
}