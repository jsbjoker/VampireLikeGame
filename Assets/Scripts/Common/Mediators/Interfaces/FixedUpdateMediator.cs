using UnityEngine;

namespace Common.Mediators.Interfaces
{
    public abstract class FixedUpdateMediator : MonoBehaviour
    {
        public abstract void AddFixedUpdatable(IFixedUpdatable fixedUpdatable);
        public abstract void RemoveFixedUpdatable(IFixedUpdatable fixedUpdatable);
    }
}