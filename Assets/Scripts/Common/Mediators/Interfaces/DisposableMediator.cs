using System;
using UnityEngine;

namespace Common.Mediators.Interfaces
{
    public abstract class DisposableMediator : MonoBehaviour
    {
        public abstract void AddDisposable(IDisposable disposable);
    }
}