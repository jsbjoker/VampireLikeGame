using UnityEngine;

namespace Common.Mediators.Interfaces
{
    public abstract class MediatorFacade : MonoBehaviour
    {
        public abstract FixedUpdateMediator FixedUpdateMediator { get; }
        public abstract UpdateMediator UpdateMediator { get; }
        public abstract DisposableMediator DisposableMediator { get; }
    }
}