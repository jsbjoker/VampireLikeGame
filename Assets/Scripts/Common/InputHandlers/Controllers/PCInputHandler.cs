using System;
using System.Collections.Generic;
using Common.InputHandlers.Enums;
using Common.InputHandlers.Interfaces;
using UnityEngine;

namespace Common.InputHandlers.Controllers
{
    public class PCInputHandler : InputHandler
    {
        private const int LeftMouseButton = 0;

        private readonly Dictionary<KeyCode, EInputButton> _inputButtons = new()
        {
            [KeyCode.W] = EInputButton.Up,
            [KeyCode.S] = EInputButton.Down,
            [KeyCode.A] = EInputButton.Left,
            [KeyCode.D] = EInputButton.Right
        };
        
        public override event Action<EInputButton> InputDown;
        public override event Action<EInputButton> InputUp;

        private void Update()
        {
            foreach (var inputButton in _inputButtons)
            {
                CheckDownKey(inputButton.Key);
                CheckUpKey(inputButton.Key);
            }
        }
        
        private void CheckDownKey(KeyCode keyCode)
        {
            if (!Input.GetKeyDown(keyCode)) return;
            InputDown?.Invoke(_inputButtons[keyCode]);
        }

        private void CheckUpKey(KeyCode keyCode)
        {
            if (!Input.GetKeyUp(keyCode)) return;
            InputUp?.Invoke(_inputButtons[keyCode]);
        }

        private void OnDestroy()
        {
            InputDown = null;
            InputUp = null;
        }
    }
}