using System;
using Common.InputHandlers.Enums;

namespace Common.InputHandlers.Interfaces
{
    public interface IInputHandler
    {
        event Action<EInputButton> InputDown;
        event Action<EInputButton> InputUp;
    }
}