using System;
using Common.InputHandlers.Enums;
using UnityEngine;

namespace Common.InputHandlers.Interfaces
{
    public abstract class InputHandler : MonoBehaviour, IInputHandler
    {
        public abstract event Action<EInputButton> InputDown;
        public abstract event Action<EInputButton> InputUp;
    }
}