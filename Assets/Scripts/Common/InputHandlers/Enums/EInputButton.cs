namespace Common.InputHandlers.Enums
{
    public enum EInputButton
    {
        Up,
        Down,
        Left,
        Right
    }
}